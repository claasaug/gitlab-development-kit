# Set up GitLab Docs

Our CI/CD pipelines include [some documentation checks](https://docs.gitlab.com/ee/development/documentation/index.html#testing)
for the documentation in GitLab. To run the links checks locally or preview the changes:

1. Pull the `gitlab-docs` repository from within your GDK directory and build dependencies:

   ```shell
   make gitlab-docs-setup
   ```

1. Change directory:

   ```shell
   cd gitlab-docs/
   ```

1. Create the HTML files:

   ```shell
   bundle exec nanoc
   ```

1. Run the internal links check:

   ```shell
   bundle exec nanoc check internal_links
   ```

1. Run the internal anchors check:

   ```shell
   bundle exec nanoc check internal_anchors
   ```

1. (Optionally) Preview the docs site locally:

   ```shell
   bundle exec nanoc live -p 3005
   ```

   Visit <http://127.0.0.1:3005/ee/README.html>.

   If you see the following message, another process is already listening on port `3005`:

   ```shell
   Address already in use - bind(2) for 127.0.0.1:3005 (Errno::EADDRINUSE)`
   ```

   Select another port and try again.
